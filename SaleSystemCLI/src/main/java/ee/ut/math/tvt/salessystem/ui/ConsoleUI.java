package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void printTeamInfo() {
        try {
            Properties prop = new Properties();
            prop.load((ConsoleUI.class.getResourceAsStream("/application.properties")));
            System.out.println("Team name: " + prop.getProperty("teamName"));
            System.out.println("Team leader: " + prop.getProperty("teamLeader"));
            System.out.println("Team leader's email: " + prop.getProperty("teamLeaderEmail"));
            System.out.println("Team members: " + prop.getProperty("teamMembers"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void addItemToStock() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the product's barcode");
        String barcode = input.nextLine();
        barcode = barcode.toLowerCase();
        List<StockItem> stockItems = dao.findStockItems();
        List<String> names = new ArrayList<>();
        List<Long> ids = new ArrayList<>();
        for (StockItem el : stockItems) {
            names.add(el.getName().toLowerCase());
            ids.add(el.getId());
        }
        if (barcode.length()==0){
            System.out.println("You did not insert anything. Please try again!");
        }
        else if (ids.contains(Long.parseLong(barcode))) {
            System.out.println("An item with this barcode already exists in stock");
        } else {
            System.out.println("Enter the product's name");
            String name = input.nextLine();
            name = name.toLowerCase();
            if(name.length()==0){
                System.out.println("You did not insert anything. Please try again!");
            }
            else if (!names.contains(name)) {
                System.out.println("Enter the product's price");
                String price = input.nextLine();
                if(price.length()==0){
                    System.out.println("You did not insert anything. Please try again!");
                }
                else if ((Double.parseDouble(price) > 0) == false) {
                    System.out.println("Price must be a positive value");
                } else {
                    System.out.println("Enter the product's quantity");
                    String quantity = input.nextLine();
                    if(quantity.length()==0){
                        System.out.println("You did not insert anything. Please try again!");
                    }
                    else if ((Integer.parseInt(quantity) > 0) == false) {
                        System.out.println("Quantity must be a positive value");
                    } else {
                        StockItem newItem = new StockItem(Long.parseLong(barcode), name, "", Double.parseDouble(price), Integer.parseInt(quantity));
                        dao.saveStockItem(newItem);
                        log.info("Item " + newItem.getName() + " saved.");
                        showStock();
                    }
                }
            } else {
                System.out.println("An item with this name already exists in stock");
                System.out.println("Would you like to update the product's information or cancel (y/n)");
                String choice = input.nextLine();
                if (choice.equals("y")){
                    System.out.println("Enter the product's price");
                    String price = input.nextLine();
                    if(price.length()==0){
                        System.out.println("You did not insert anything. Please try again!");
                    }
                    else if ((Double.parseDouble(price) > 0) == false) {
                        System.out.println("Price must be a positive value");
                    } else {
                        System.out.println("Enter the product's quantity");
                        String quantity = input.nextLine();
                        if(quantity.length()==0){
                            System.out.println("You did not insert anything. Please try again!");
                        }
                        else if ((Double.parseDouble(quantity) > 0) == false) {
                            System.out.println("Quantity must be a positive value");
                        } else {
                            for (StockItem el:stockItems) {
                                if (el.getName().toLowerCase().equals(name)){
                                    el.setPrice(Double.parseDouble(price));
                                    el.setQuantity(el.getQuantity() + Integer.parseInt(quantity));
                                    log.info("Item " + el.getName() + " modified.");
                                }
                            }
                            showStock();
                        }
                    }
                } else {
                    System.out.println("Cancelled adding an item.");
                }
            }
        }
    }

    private void deleteItemFromStock(){
        showStock();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the product's name");
        String name = input.nextLine();
        name = name.toLowerCase();
        StockItem item = dao.findStockItemByName(name);
        dao.deleteStockItem(item);
        log.info("Item " + name + " removed from stock.");
        showStock();
    }

    public void showTotalPrice(){
        double totalPrice = 0.0;
        for (SoldItem solditem: cart.getAll()) {
            totalPrice += (solditem.getPrice()*solditem.getQuantity());
        }
        System.out.println("Total price of cart is: ");
    }

    public void printProductInfo(StockItem stockitem){
        System.out.println("Product's info:");
        System.out.println("Barcode: "+stockitem.getId());
        System.out.println("Name:" +stockitem.getName());
        System.out.println("Price: "+stockitem.getPrice());
        System.out.println("Available in stock: "+stockitem.getQuantity());
    }

    public void searchItem(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter product's barcode or name: ");
        String inserted = input.nextLine().toLowerCase();
        String output = "";
        char[] chars = inserted.toCharArray();
        List<StockItem> stockItems = dao.findStockItems();
            if (inserted.length()==0){
                System.out.println("You did not insert anything. Please try again.");
            }
            else if (Character.isDigit(chars[0])) {
                Long id = Long.parseLong(inserted);
                for (StockItem stockitem: stockItems
                     ) {

                    if (id.equals(stockitem.getId())){
                        printProductInfo(stockitem);
                        output = "";
                        break;
                    }
                    else {
                        output = "The product that you looked for was not found in the warehouse. Please try again!";
                    }
                }
            }
            else if (!Character.isDigit(chars[0])) {
                for (StockItem stockitem: stockItems
                     ) {
                    if (inserted.equals(stockitem.getName())){
                        printProductInfo(stockitem);
                        output = "";
                        break;
                    }
                    else {
                        output = "The product that you looked for was not found in the warehouse. Please try again!";
                    }
                }
            }
                System.out.println(output);
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("c\t\tShow cart contents");
        System.out.println("total\t\tShow total price of the cart");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("t\t\tShow team information");
        System.out.println("add\t\tAdd product to stock");
        System.out.println("del\t\tDelete product from stock");
        System.out.println("s\t\tSearch item from stock");
        System.out.println("-------------------------");
    }



    private void processCommand(String command) {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("p"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("t"))
            printTeamInfo();
        else if (c[0].equals("add")) {
            addItemToStock();
        } else if (c[0].equals("del")) {
            deleteItemFromStock();
        } else if (c[0].equals("total")) {
            showTotalPrice();
        } else if (c[0].equals("s")){
            searchItem();
        }

        else if (c[0].equals("a") && c.length == 3) {
            try {
                long idx = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.findStockItem(idx);
                if (item != null) {
                    cart.addItem(new SoldItem(item, Math.min(amount, item.getQuantity())));
                } else {
                    System.out.println("No stock item with this id " + idx);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            System.out.println("Unknown command");
        }
    }

}
