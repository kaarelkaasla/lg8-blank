# Team lg8-blank:
1. Kaarel Kaasla
2. Kristiina Kuningas

## Homework 1:
Here is the link to our first homework:
https://bitbucket.org/kaarelkaasla/lg8-blank/wiki/Homework%201%20-%20Requirements%20Gathering

## Homework 2:
Here is the link to our second homework:
https://bitbucket.org/kaarelkaasla/lg8-blank/wiki/Homework%202%20-%20Requirements%20Specification,%20Modeling,%20Planning

## Homework 3:
https://bitbucket.org/kaarelkaasla/lg8-blank/commits/tag/homework-3

## Homework 4:
https://bitbucket.org/kaarelkaasla/lg8-blank/wiki/Homework%204

https://bitbucket.org/kaarelkaasla/lg8-blank/commits/tag/homework-4

## Homework 5:
https://bitbucket.org/kaarelkaasla/lg8-blank/commits/tag/homework-5-2

## Homework 6:

https://bitbucket.org/kaarelkaasla/lg8-blank/commits/tag/homework-6

Test plan : https://docs.google.com/spreadsheets/d/1rNbrIg_eQA_JC_QUnUEh6TVsmp2If-aa6ug8hkrC1hA/edit?usp=sharing

Test cases: https://docs.google.com/spreadsheets/d/1c4x4zMEzDFAMBRqTvknJEQM1us6SSvI_ff8fm9-rcKw/edit?usp=sharing

Refactoring : https://bitbucket.org/kaarelkaasla/lg8-blank/commits/20e7c614e2475ee826c2b15747c95a61ddee91d2
Added the detailed information of the refactoring process to the comments section of the commit.

## Homework 7:

Team test report: https://docs.google.com/document/d/1XcC6KEsxZSQz-eUm3vQUc14AeHRQC4NDhj2Gjo_bYaU/edit

Our zip files: https://drive.google.com/drive/folders/1CgfkYdav6XjBlHTVTiDLLBZVLgne3MZY?usp=sharing

Test reports: https://docs.google.com/spreadsheets/d/1c4x4zMEzDFAMBRqTvknJEQM1us6SSvI_ff8fm9-rcKw/edit#gid=1373217018

Usability tests: https://docs.google.com/document/d/1tWsbb8XANlXh565C-jjlFwJ7TuU8MRcIWV3sPAViPU8/edit

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)