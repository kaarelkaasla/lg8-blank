package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    private static final Logger log = LogManager.getLogger(TeamController.class);
    @FXML
    private Text teamName;
    @FXML
    private Text teamLeader;
    @FXML
    private Text teamLeaderEmail;
    @FXML
    private Text teamMembers;
    @FXML
    private ImageView teamLogo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try (InputStream propertyStream = getClass().getClassLoader().getResourceAsStream("application.properties")) {
            Properties prop = new Properties();
            prop.load(propertyStream);
            teamName.setText(prop.getProperty("teamName"));
            teamLeader.setText(prop.getProperty("teamLeader"));
            teamLeaderEmail.setText(prop.getProperty("teamLeaderEmail"));
            teamMembers.setText(prop.getProperty("teamMembers"));
            teamLogo.setImage(new Image(prop.getProperty("teamLogo")));
            log.info("Team controller initialized");
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
