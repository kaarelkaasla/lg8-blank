package ee.ut.math.tvt.salessystem.ui.controllers;

//import com.sun.javafx.css.StyleManager;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.StockManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private VBox newWindow;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private Label totalPriceCounter;
    @FXML
    private Button loginButton;
    @FXML
    private Button deleteItem;


    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Purchase tab initialized");

        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAll()));
        deleteItem.setOnAction(e -> removeItemFromTableview());
        disableProductField(true);
        submitPurchase.setOnAction(e -> submitPurchaseButtonClicked());

        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    public void removeItemFromTableview() {
        double counter3 = 0;
        SoldItem selectedItem = purchaseTableView.getSelectionModel().getSelectedItem();
        System.out.println(selectedItem);
        purchaseTableView.getItems().remove(selectedItem);
        for (SoldItem itemsAfter: shoppingCart.getAll()
             ) {
            counter3 += itemsAfter.getSum();
        }
        dao.changeItemQuantity(selectedItem.getId(),selectedItem.getQuantity());
        totalPriceCounter.setText(Double.toString(counter3));
    }


    /*public void ifCustomerWantsInvoice() {
        final Stage dialog = new Stage();
        Button no = new Button("NO");
        VBox dialogVbox = new VBox(new Text("Do you want an invoice of your purchase?"), new Button("YES"), no);
        no.setOnAction(e -> dialog.close());
        dialogVbox.setPadding(new Insets(15));
        Scene dialogScene = new Scene(dialogVbox, 300, 200);
        dialog.setScene(dialogScene);
        dialog.show();
    }*/

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
            log.info("Sale complete");
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
        log.debug("Purchase completed");

    }

    @FXML

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        deleteItem.setDisable(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
        log.debug("Inputs enabled");
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        deleteItem.setDisable(true);
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
        log.debug("Inputs disabled");
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
            log.debug("Text fields filled");
        } else {
            resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        double counter2 = 0;
        //add chosen item to the shopping cart.
        try {
            log.debug("A product has been added to the cart.");
            StockItem stockItem = getStockItemByBarcode();
            if (stockItem != null) {
                int quantity;
                try {
                    quantity = Integer.parseInt(quantityField.getText());
                } catch (NumberFormatException e) {
                    quantity = 1;
                }
                if(dao.findStockItem(stockItem.getId()).getQuantity()>0){
                    shoppingCart.addItem(new SoldItem(stockItem, quantity));
                    dao.lowerItemQuantity(stockItem.getId(),quantity);
                    for (SoldItem itemCount: shoppingCart.getAll()
                    ) {
                            counter2 += itemCount.getSum();
                    }
                    totalPriceCounter.setText(Double.toString(counter2));
                }
                else {
                    System.out.println("ERROR. There is no products of this kind left in the warehouse.");
                }
                purchaseTableView.refresh();
            }
        } catch (Exception e){
            log.error(e.getMessage());
        }
        log.debug("Item added");
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
        this.priceField.setDisable(disable);
        log.debug("Product fields disabled");
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setText("");
        priceField.setText("");
        totalPriceCounter.setText("");
        log.debug("Product fields reseted");
    }
}
