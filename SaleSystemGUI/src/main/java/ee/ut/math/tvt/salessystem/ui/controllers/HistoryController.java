package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.collections.FXCollections;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final SalesSystemDAO dao;

    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private Button showBetweenDates;
    @FXML
    private Button showLast10;
    @FXML
    private Button showAll;
    @FXML
    private TableView<Purchase> historyTableViewTime;
    @FXML
    private TableView<SoldItem> historyTableViewPurchase;

    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showAll.setOnAction(e->historyTableViewTime.setItems(FXCollections.observableList(dao.findAllPurchases())));
        showLast10.setOnAction(e->historyTableViewTime.setItems(FXCollections.observableList(dao.findLastPurchases(10))));
        showBetweenDates.setOnAction(e->historyTableViewTime.setItems(FXCollections.observableList(dao.findPurchasesBetween(startDate.getValue(), endDate.getValue()))));
        historyTableViewTime.getSelectionModel().selectedItemProperty().addListener((value, old, n)->displayContents(n));
        log.info("HistoryController initialized");
    }

    private void displayContents(Purchase purchase) {
        if (purchase == null) {
            historyTableViewPurchase.setItems(FXCollections.observableList(new ArrayList<>()));
            log.debug("History table displays contents");
            return;
        }
        historyTableViewPurchase.setItems(FXCollections.observableList(dao.getSoldItemsList(purchase)));
        historyTableViewPurchase.refresh();
        log.debug("History table displays contents");
    }
}
