package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.StockManager;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;
    private final StockManager manager;
    private static final Logger log = LogManager.getLogger(StockController.class);

    @FXML
    private TextField barCodeField;
    @FXML
    private TextField amountField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField searchField;
    @FXML
    private Button addItem;
    @FXML
    private Button refreshWarehouse;
    @FXML
    private Button deleteItem;
    @FXML
    private Button searchItem;
    @FXML
    private Label answerToSearch;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
        manager = new StockManager(dao);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("Stock tab initialized");
        refreshStockItems();
        addItem.setOnAction(e -> addItem());
        searchItem.setOnAction(e -> searchItem());
        refreshWarehouse.setOnAction(e -> refreshButtonClicked());

        deleteItem.setOnAction(e -> {
            log.debug("Remove button clicked");
            StockItem selectedItem = warehouseTableView.getSelectionModel().getSelectedItem();
            warehouseTableView.getItems().remove(selectedItem);
            dao.deleteStockItem(selectedItem);
        });
    }

    public void searchItem() {
        Stage stage = new Stage();
        Label productInfo = new Label();
        String searchedItem = searchField.getText().toLowerCase();
        char[] chars = searchedItem.toCharArray();
        if (searchField.getText().length() == 0) {
            productInfo.setText("Must insert the name or " + "\nthe barcode of a product. " + "\nPlease try again!");
        } else if (Character.isDigit(chars[0])) {
            long id = Long.parseLong(searchedItem);
            for (StockItem stockitem : dao.findStockItems()) {
                if (stockitem.getId() == id) {
                    productInfo.setText(" Id: " + dao.findStockItem(id).getId() + "\n Name: " + dao.findStockItem(id).getName() + "\n Price: " + dao.findStockItem(id).getPrice() + "\n Available in stock: " + dao.findStockItem(id).getQuantity() + " items");
                    break;
                } else {
                    productInfo.setText("The product that you are looking for " + "\nwas not found in the warehouse.");
                }
            }
        } else if (!Character.isDigit(chars[0])) {
            for (StockItem stockitem : dao.findStockItems()
            ) {
                if (stockitem.getName().equals(searchedItem)) {
                    productInfo.setText(" Id: " + stockitem.getId() + "\n Name: " + stockitem.getName() + "\n Price: " + stockitem.getPrice() + "\n Available in stock: " + stockitem.getQuantity() + " items");
                    break;
                } else {
                    productInfo.setText("The product that you are looking for " + "\nwas not found in the warehouse.");
                }
            }
        }

        productInfo.setFont(Font.font("verdana", 15));
        productInfo.setTextFill(Color.web("#FFEBCD"));
        Group root = new Group();
        root.getChildren().add(productInfo);
        Scene scene = new Scene(root, 300, 100, Color.web("#5FA292"));
        stage.setTitle("Product information");
        stage.setScene(scene);
        stage.show();

    }

    public void addItem() {
        log.debug("Add button clicked");
        if ("".equals(barCodeField.getText()) || (Long.parseLong(barCodeField.getText()) > 0) == false) {
            Alert barcodeError = new Alert(Alert.AlertType.ERROR);
            barcodeError.setTitle("Invalid barcode");
            barcodeError.setHeaderText("Barcode must be a positive value");
            barcodeError.showAndWait();
        } else {
            if ("".equals(priceField.getText()) || (Double.parseDouble(priceField.getText()) > 0)==false) {
                Alert priceError = new Alert(Alert.AlertType.ERROR);
                priceError.setTitle("Invalid price");
                priceError.setHeaderText("Price must be a positive value");
                priceError.showAndWait();

            } else {
                if ("".equals(amountField.getText()) || (Integer.parseInt(amountField.getText()) > 0) == false) {
                    Alert quantityError = new Alert(Alert.AlertType.ERROR);
                    quantityError.setTitle("Invalid quantity");
                    quantityError.setHeaderText("Quantity must be a positive value");
                    quantityError.showAndWait();
                } else {
                    String name = nameField.getText().toLowerCase();
                    List<StockItem> items = dao.findStockItems();
                    List<String> names = new ArrayList<>();
                    List<Long> ids = new ArrayList<>();
                    for (StockItem el : items) {
                        names.add(el.getName().toLowerCase());
                        ids.add(el.getId());
                    }
                    if (ids.contains(Long.parseLong(barCodeField.getText()))) {
                        Alert barcodeError = new Alert(Alert.AlertType.ERROR);
                        barcodeError.setTitle("Duplicate barcode");
                        barcodeError.setHeaderText("An item with this barcode already exists in stock");
                        barcodeError.showAndWait();

                    } else {
                        if (!names.contains(name)) {
                            if ("".equals(name)) {
                                Alert barcodeError = new Alert(Alert.AlertType.ERROR);
                                barcodeError.setTitle("Invalid name");
                                barcodeError.setHeaderText("Cannot add an item without a name");
                                barcodeError.showAndWait();
                            } else {
                                manager.addItem(barCodeField.getText(), nameField.getText(), priceField.getText(), amountField.getText());
                                barCodeField.clear();
                                nameField.clear();
                                priceField.clear();
                                amountField.clear();
                                log.debug("Added an item to stock");
                                refreshStockItems();
                            }
                        } else {
                            Alert duplicateError = new Alert(Alert.AlertType.CONFIRMATION);
                            duplicateError.setTitle("Duplicate item");
                            duplicateError.setHeaderText("This item is already in stock");
                            duplicateError.setContentText("Would you like to update the item information?");

                            Optional<ButtonType> result = duplicateError.showAndWait();
                            if (result.get() == ButtonType.OK) {
                                manager.changeItem(nameField.getText(), priceField.getText(), amountField.getText());
                                barCodeField.clear();
                                nameField.clear();
                                priceField.clear();
                                amountField.clear();
                                log.debug("Changed the item price/quantity");
                                refreshStockItems();
                            } else {
                                barCodeField.clear();
                                nameField.clear();
                                priceField.clear();
                                amountField.clear();
                                log.debug("Canceled adding an item");
                                refreshStockItems();
                            }
                        }
                    }
                }
            }
        }
        refreshStockItems();
    }

    @FXML
    public void refreshButtonClicked() {
        log.debug("Refresh button clicked");
        refreshStockItems();
    }

    private void refreshStockItems() {
        log.debug("Warehouse refreshed");
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
    }
}
