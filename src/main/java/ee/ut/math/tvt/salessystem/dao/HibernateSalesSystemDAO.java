package ee.ut.math.tvt.salessystem.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public boolean isBeginActive() {
        return false;
    }

    @Override
    public boolean isCommitActive() {
        return false;
    }

    @Override
    public boolean isBeginBefore() {
        return false;
    }

    @Override
    public void numberOfCalls() {
    }

    @Override
    public void initSavedStockItem() {
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("from StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    @Override
    public StockItem findStockItemByName(String name) {
        return em.find(StockItem.class, name);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        em.merge(stockItem);
    }

    @Override
    public void deleteStockItem(StockItem item) {
        beginTransaction();
        em.remove(item);
        commitTransaction();
    }

    @Override
    public void changeItemQuantity(long id, int quantity) {
        StockItem item = findStockItem(id);
        item.addToQuantity(quantity);
        em.merge(item);
    }

    @Override
    public void lowerItemQuantity(long id, int quantity) {
    }

    @Override
    public void saveSoldItem(SoldItem item) {
    }

    public void saveSoldItems(List<SoldItem> items, double sum) {
        Purchase newPurchase = new Purchase();
        newPurchase.setSum(sum);
        newPurchase.setPurchaseTime(LocalDateTime.now());
        newPurchase = em.merge(newPurchase);
        for (SoldItem item: items) {
            item.setPurchase(newPurchase);
            em.merge(item);
        }
    }

    public List<SoldItem> getSoldItemsList(Purchase purchase) {
        return em.createQuery("from SoldItem i where i.purchase.id = :p_id", SoldItem.class).setParameter("p_id", purchase.getId().longValue()).getResultList();
    }

    public List<Purchase> findPurchasesBetween(LocalDate startDate, LocalDate endDate) {
        return em.createQuery("from Purchase where PURCHASE_TIME between :start and :end", Purchase.class)
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .getResultList();
    }

    public List<Purchase> findAllPurchases() {
        return em.createQuery("from Purchase", Purchase.class).getResultList();
    }

    public List<Purchase> findLastPurchases(int n) {
        return em.createQuery("from Purchase order by PURCHASE_TIME desc", Purchase.class).setMaxResults(n).getResultList();
    }

}