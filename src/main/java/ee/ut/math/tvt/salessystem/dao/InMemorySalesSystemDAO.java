package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private  List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final Map<Purchase, List<SoldItem>> soldItemMap;
    private final List<Purchase> purchaseList;
    public boolean transactionActive = false;
    public int numberOfStartedTransactions = 0;
    public int numberOfCommitedTransactions = 0;
    public boolean beginActive = false;
    public boolean commitActive = false;
    public int numberOfMethodCalls = 0;
    public boolean savedThroughDAO = false;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "lays chips", "potato chips", 11.0, 5));
        items.add(new StockItem(2L, "chupa-chups", "sweets", 8.0, 8));
        items.add(new StockItem(3L, "frankfurters", "beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "free beer", "student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.soldItemMap = new HashMap<>();
        purchaseList = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }
    public StockItem findStockItemByName(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public void deleteStockItem(StockItem item) {
//        stockItemList = stockItemList.stream().filter(item -> !item.getName().equals(elem)).collect(Collectors.toList());
//        return stockItemList;
    }

    @Override
    public void saveSoldItems(List<SoldItem> items, double sum) {
        Purchase newPurchase = new Purchase();
        newPurchase.setId((long) purchaseList.size());
        newPurchase.setSum(sum);
        newPurchase.setPurchaseTime(LocalDateTime.now());
        purchaseList.add(newPurchase);
        soldItemMap.put(newPurchase, new ArrayList<>(items));
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
        savedThroughDAO = true;
    }

    @Override
    public void beginTransaction() {
        if (transactionActive)
            throw new RuntimeException("transaction already in progress");
        transactionActive = true;
        numberOfStartedTransactions += 1;
        numberOfCalls();
    }

    @Override
    public void rollbackTransaction() {
        if (!transactionActive)
            throw new RuntimeException("transaction not started");
        transactionActive = false;
    }

    @Override
    public void commitTransaction() {
        if (!transactionActive)
            throw new RuntimeException("transaction not started");
        transactionActive = false;
        numberOfCommitedTransactions += 1;
        numberOfCalls();
    }

    @Override
    public void changeItemQuantity(long id, int quantity) {
        findStockItem(id).setQuantity(quantity+findStockItem(id).getQuantity());
    }

    public void lowerItemQuantity(long id, int quantity) {
        findStockItem(id).setQuantity(findStockItem(id).getQuantity()-quantity);
    }

    @Override
    public boolean isBeginActive(){
        return true;
    }

    @Override
    public boolean isCommitActive(){
        if (isBeginActive()){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean isBeginBefore(){
        if (isBeginActive()){
            if (isCommitActive()){
                return true;
            } return false;
        }
        return false;
    }

    @Override
    public void numberOfCalls(){
        numberOfMethodCalls++;
    }

    @Override
    public void initSavedStockItem(){
        savedThroughDAO = false;
    }

    @Override
    public List<SoldItem> getSoldItemsList(Purchase purchase) {
        return soldItemMap.get(purchase);
    }

    @Override
    public List<Purchase> findPurchasesBetween(LocalDate startDate, LocalDate endDate) {
        if (startDate == null || endDate == null)
            return new ArrayList<>();
        List<Purchase> purchases = new ArrayList<>();
        for (Purchase p: purchaseList) {
            if (p.getPurchaseTime().toLocalDate().compareTo(startDate) >= 0  && p.getPurchaseTime().toLocalDate().compareTo(endDate) <= 0)
                purchases.add(p);
        }
        return purchases;
    }

    @Override
    public List<Purchase> findAllPurchases() {
        return purchaseList;
    }

    @Override
    public List<Purchase> findLastPurchases(int n) {
        return new ArrayList<Purchase>(purchaseList.subList(purchaseList.size()-n-1, purchaseList.size()));
    }

}
