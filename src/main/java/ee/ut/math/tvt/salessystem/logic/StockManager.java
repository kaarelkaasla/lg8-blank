package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.List;

public class StockManager {
    private final SalesSystemDAO dao;

    public StockManager(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addItem(String barCode, String name, String price, String quantity) {
        dao.beginTransaction();
        dao.isBeginActive();
        dao.numberOfCalls();
        try {
            if ("".equals(barCode)) {
                throw new RuntimeException("Barcode field empty");
            }
            StockItem previous = dao.findStockItem(Long.parseLong(barCode));
            if (previous != null) {
                dao.initSavedStockItem();
                if (!"".equals(name) && !previous.getName().equals(name)) {
                    throw new RuntimeException("Invalid name");
                }
                if (!"".equals(price) && previous.getPrice() != Double.parseDouble(price)) {
                    throw new RuntimeException("Invalid price");
                }
                if (previous.getQuantity() + Integer.parseInt(quantity) < 0) {
                    throw new RuntimeException("Quantity cannot be below 0");
                }
                dao.changeItemQuantity(Long.parseLong(barCode), Integer.parseInt(quantity));
            } else {
                if (Double.parseDouble(price) < 0) {
                    throw new RuntimeException("Invalid price");
                }
                if (Integer.parseInt(quantity) < 0) {
                    throw new RuntimeException("Invalid quantity");
                }
                StockItem newItem = new StockItem(Long.parseLong(barCode), name, "", Double.parseDouble(price), Integer.parseInt(quantity));
                dao.saveStockItem(newItem);
            }
            dao.commitTransaction();
            dao.isCommitActive();
            dao.isBeginBefore();
            dao.numberOfCalls();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public void changeItem(String name, String price, String quantity) {
        List<StockItem> items = dao.findStockItems();
        for (StockItem el : items) {
            if (el.getName().toLowerCase().equals(name)) {
                el.setPrice(Double.parseDouble(price));
                el.setQuantity(el.getQuantity() + Integer.parseInt(quantity));
            }
        }
    }
}