package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        SoldItem existing = null;
        int quantity = item.getQuantity();
        for (SoldItem solditem: items) {
            if (solditem.getId() == item.getId()) {
                existing = solditem;
                quantity += existing.getQuantity();
            }
        }
        if (quantity < 0) {
            throw new RuntimeException("Quantity negative");
        }
        if(quantity > item.getStockItem().getQuantity()){
            throw new RuntimeException("Quantity exceeded");
        }
        if (existing == null)
            items.add(item);
        else {
            existing.setQuantity(quantity);
        }
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            double sum = 0;
            for (SoldItem item : items) {
                sum += item.getSum();
                dao.changeItemQuantity(item.getStockItem().getId(), -item.getQuantity());
            }
            dao.saveSoldItems(items, sum);
            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

}