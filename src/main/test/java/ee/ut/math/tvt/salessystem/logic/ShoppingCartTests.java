package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class ShoppingCartTests {
    private InMemorySalesSystemDAO dao;
    private StockItem stock;
    private ShoppingCart cart;

    @Before
    public void setUp() {
        dao = new InMemorySalesSystemDAO();
        stock = dao.findStockItem(1);
        cart = new ShoppingCart(dao);
    }

    //addItem
    @Test
    public void testAddingExistingItem() {
        SoldItem item = new SoldItem(stock, 1);
        //checks quantity of the first added item
        assertEquals(1, (long)item.getQuantity());
        SoldItem item1 = new SoldItem(stock, 1);
        //checks quantity of the second added item
        assertEquals(1, (long)item1.getQuantity());
        //adds both instances of the same item to the cart
        cart.addItem(item);
        cart.addItem(item1);
        //checks if the cart now consists of only one item and not both
        assertEquals(1, cart.getAll().size());
        //if identical item is added, it's quantity is added to the first instance
        //checks whether this is true
        assertEquals(2, (long)item.getQuantity());
    }

    @Test
    public void testAddingNewItem() {
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        assertEquals(1, cart.getAll().size());
    }

    @Test(expected = Exception.class)
    public void testAddingItemWithNegativeQuantity() {
        SoldItem item = new SoldItem(stock, -1);
        cart.addItem(item);
    }

    @Test(expected = Exception.class)
    public void testAddingItemWithQuantityTooLarge() {
        SoldItem item = new SoldItem(stock, stock.getQuantity() + 1);
        cart.addItem(item);
    }

    @Test(expected = Exception.class)
    public void testAddingItemWithQuantitySumTooLarge() {
        SoldItem item = new SoldItem(stock, 1);
        SoldItem item1 = new SoldItem(stock, stock.getQuantity() + 1);
        cart.addItem(item);
        cart.addItem(item1);
    }

    //submitCurrentPurchase
    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        int before = stock.getQuantity();
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        cart.submitCurrentPurchase();
        assertEquals(before - 1, item.getStockItem().getQuantity());
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        cart.submitCurrentPurchase();
        assertEquals(1, dao.numberOfStartedTransactions);
        assertEquals(1, dao.numberOfCommitedTransactions);
        assertEquals("should return 2 calls (1 for both methods)", 2, dao.numberOfMethodCalls);
        Assert.assertTrue("should return true that begin executes before commit", dao.isBeginBefore());
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        cart.submitCurrentPurchase();
        //findAllPurchases stores history items
        assertEquals(1, dao.findAllPurchases().size());
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        cart.submitCurrentPurchase();
        Purchase purchase = dao.findAllPurchases().get(0);
        LocalDateTime time = LocalDateTime.now();
        //checks whether the saved time is between the bounds -10 and +10 seconds to the current time
        assertTrue(time.minusSeconds(10).isBefore(purchase.getPurchaseTime()));
        assertTrue(time.plusSeconds(10).isAfter(purchase.getPurchaseTime()));
    }

    @Test
    public void testCancellingOrder() {
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        cart.cancelCurrentPurchase();
        //adds different item than before
        SoldItem item1 = new SoldItem(dao.findStockItem(2), 2);
        cart.addItem(item1);
        //checks the number of items in the cart, should have only one item
        assertEquals(1, cart.getAll().size());
        //checks whether this one item is the same as the second added item
        assertEquals(item1, cart.getAll().get(0));
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        int before = stock.getQuantity();
        SoldItem item = new SoldItem(stock, 1);
        cart.addItem(item);
        cart.cancelCurrentPurchase();
        assertEquals(before, item.getStockItem().getQuantity());
    }
}
