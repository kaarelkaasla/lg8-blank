package ee.ut.math.tvt.salessystem.logic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;


//NOTE: RUN THE TESTS THROUGH INTELLIJ AND NOT GRADLE!
public class WarehouseTests {
    InMemorySalesSystemDAO dao;
    StockManager manager;

    @Before
    public void init() {
        dao = new InMemorySalesSystemDAO();
        manager = new StockManager(dao);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        manager.addItem("100", "testItem", "10", "1");
        Assert.assertFalse("should have ended transaction", dao.transactionActive);
        Assert.assertEquals("should have made exactly 1 transaction", 1, dao.numberOfCommitedTransactions);
        //checks that the methods are called in order
        Assert.assertTrue("should return true that begin executes before commit", dao.isBeginBefore());
        //checks that there are 2 method calls in total which means that each method is called one time
        Assert.assertEquals("should return 2 calls (1 for both methods)", 2, dao.numberOfMethodCalls);
    }

    @Test
    public void testAddingNewItem() {
        manager.addItem("100", "testItem", "10", "1");
        StockItem item = dao.findStockItem(100);
        Assert.assertNotNull(item);
        Assert.assertEquals("testItem", item.getName());
        Assert.assertEquals(1, item.getQuantity());
        //checks if saveStockItem is used
        Assert.assertTrue("should return true if was saved through DAO", dao.savedThroughDAO);

    }

    @Test
    public void testAddingExistingItem() {
        manager.addItem("100", "testItem", "10", "1");
        manager.addItem("100", "testItem", "10", "1");
        StockItem item = dao.findStockItem(100);
        Assert.assertNotNull(item);
        Assert.assertEquals("testItem", item.getName());
        Assert.assertEquals(2, item.getQuantity());
        //tests if wasn't saved through DAO
        Assert.assertFalse("test should pass if wasn't saved through DAO", dao.savedThroughDAO);
    }

    @Test(expected = RuntimeException.class)
    public void testAddingItemWithNegativeQuantity() {
        manager.addItem("100", "name", "1.50", "-10");
    }

    @Test(expected = RuntimeException.class)
    public void testAddingItemsWithMissingArguments() {
        manager.addItem("", "", "1", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddingItemWithInvalidArguments() {
        manager.addItem("10", "testItem", "-", "-");
    }
}
